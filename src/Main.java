import java.util.concurrent.Semaphore;

public class Main {

	static int counter = 0;
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("HELLO WORLD!");
	
		// Guard a bathroom with 1 toilet
		Semaphore receptionist = new Semaphore(1);
		
		Thread t1 = new Thread("t1") {
		
			public void run() {
				
				String threadName = Thread.currentThread().getName();
				
				System.out.println(threadName + 
						" is going to the bathroom");
				
				try {
					System.out.println(threadName 
							+ " is asking for permission to enter the bathroom");
					
					receptionist.acquire();
					
					System.out.println(threadName
							+ " got permission! Entering bathroom.");
					
					// do something in the bathroom
					for (int i = 0; i < 10; i++) {
						counter++;
						System.out.println(threadName + ": counter = " + counter);
						
						// Mimic a slow process
						// (eg: mimic using the toilet for a long time)
						Thread.sleep(10);
					}
					
					System.out.println(threadName + " is done using the bathroom. Notifying receptionist");
					
					receptionist.release();
					
					System.out.println(threadName + " is done.");
					
				}
				catch (InterruptedException e) {
					System.out.println(e.getLocalizedMessage());
				}
			}
		}; 	// end Thread 1
		
		
		Thread t2 = new Thread("t2") {
			
			public void run() {
				
				String threadName = Thread.currentThread().getName();
				
				System.out.println(threadName + 
						" is going to the bathroom");
				
				try {
					System.out.println(threadName 
							+ " is asking for permission to enter the bathroom");
					
					receptionist.acquire();
					
					System.out.println(threadName
							+ " got permission! Entering bathroom.");
					
					// do something in the bathroom
					for (int i = 0; i < 10; i++) {
						counter--;
						
						System.out.println(threadName + ": counter = " + counter);
						// Mimic a slow process
						// (eg: mimic using the toilet for a long time)
						Thread.sleep(10);
					}
					
					System.out.println(threadName + " is done using the bathroom. Notifying receptionist");
					
					receptionist.release();
					
					System.out.println(threadName + " is done.");
					
				}
				catch (InterruptedException e) {
					System.out.println(e.getLocalizedMessage());
				}
			}
		}; 	// end Thread

		
		
		t1.start();
		t2.start();
		
		try {
			t1.join();
			t2.join();
		}
		catch(InterruptedException e) {
			
		}
		
		System.out.println("Program finished!");
		System.out.println("Final value of counter: " + counter);
	}
	

}
