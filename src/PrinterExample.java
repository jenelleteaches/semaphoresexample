import java.util.concurrent.Semaphore;


class Document {
	String user;
	String filename;
	int numPages;
	public Document(String user, String filename, int numPages) {
		this.user = user;
		this.filename = filename;
		this.numPages = numPages;
	}
	public String printDocument() {
		return this.user + " : Sending " + this.filename + " to the printer";
	}
}


class PrinterThread extends Thread {
	
	// create the semaphore - 1 semaphore to rule them all!
	final static int NUM_PRINTERS = 4;
	static volatile Semaphore printerSecurityGuard = new Semaphore(NUM_PRINTERS);
	
	// printer specific variables
	final static int PRINT_TIME_PER_PAGE = 2000;
	
	// Document to print
	Document document;
	int documentPrintingTime;
	
	public PrinterThread(String threadName, Document d) {
		super(threadName);
		this.document = d;
		this.documentPrintingTime = this.document.numPages * PRINT_TIME_PER_PAGE;
	}
	
	public void run() {
		String threadName = Thread.currentThread().getName();
	
		try {
			
//			boolean acquireSuccessful = printerSecurityGuard.tryAcquire();
//			
//			String status = "";
//			if (acquireSuccessful) {
//				status = "SUCCESS!";
//			}
//			else {
//				status = "FAILED!";
//			}
//			
//			System.out.println(threadName + " is trying to get a printer: " 
//					+ status  
//					+ " Printers available: "  
//					+ this.printerSecurityGuard.availablePermits());
//			
			
			
			System.out.println(threadName + " is trying to get a printer: " 
					+ " Printers available: "  
					+ this.printerSecurityGuard.availablePermits());
			this.printerSecurityGuard.acquire();
			
			// -------- CRITICAL SECTION -----------
			
			System.out.println(threadName + " got a printer. Printers remaining: "  
					+ this.printerSecurityGuard.availablePermits());
			System.out.println(threadName + ":\t" + this.document.printDocument());
			// mimic the time it takes to print the document
			Thread.sleep(documentPrintingTime);
			
			// -------- END CRITICAL SECTION -----------
			
			
			printerSecurityGuard.release();
			System.out.println(threadName + " finished printing and released the printer. "
					+ "Printers Available: " + this.printerSecurityGuard.availablePermits());
			
		} catch (InterruptedException e) {
			// error handling goes here
			System.out.println(threadName + " all printers are in use");
		}
	}
}


public class PrinterExample {

	
	final static int NUM_PRINTERS = 1;
	final static int PRINT_TIME_PER_PAGE = 2000;
	static Semaphore securityGuard = new Semaphore(NUM_PRINTERS);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		Document d = new Document("jenelle", "C:\\midterm.doc", 5);
		Document d2 = new Document("mstoica", "/Users/magdin/Desktop/capstone.pdf", 10);
		Document d3 = new Document("esykes", "C:\\Users\\ed\\Research\\topsecretresearch.pdf", 1);
		Document d4 = new Document("dshenoda", "assignmnent1-submission01", 2);
		Document d5 = new Document("dshenoda", "assignmnent1-submission02", 3);
		Document d6 = new Document("dshenoda", "assignmnent1-submission03", 1);
		Document d7 = new Document("dshenoda", "assignmnent1-submission04", 4);
		
		PrinterThread p1 = new PrinterThread("t1", d);	
		PrinterThread p2 = new PrinterThread("t2", d2);
		PrinterThread p3 = new PrinterThread("t3", d3);
		PrinterThread p4 = new PrinterThread("t4", d4);
		PrinterThread p5 = new PrinterThread("t5", d5);
		PrinterThread p6 = new PrinterThread("t6", d6);
		PrinterThread p7 = new PrinterThread("t7", d7);
		
		
		int numPrinters = securityGuard.availablePermits();
		
		System.out.println("There are " + numPrinters + " printers available.");
		
		
		
		
		
		p1.start();
		p2.start();
		p3.start();
		p4.start();
		p5.start();
		p6.start();
		p7.start();
		
		try {
			p1.join();
			p2.join();
			p3.join();
			p4.join();
			p5.join();
			p6.join();
			p7.join();
		}
		catch (InterruptedException e) {
		}
		
		System.out.println("------------------------");
		System.out.println("All print jobs finished.");

	}

}
